import IsKtp from "../../src/boolean/is-ktp";
import Standard from "../../src/standard";
import ToString from "lits/dist/string/to-string";

it("force console log", () => { spyOn(console, 'log').and.callThrough();});


describe("valid", function() {

    it("instantiated", function() {

        expect(IsKtp(new Standard())).toBe(true)

    });


    it("object", function() {

        let object = {
            province            : null,     regencyCity : null,     issued      : null,
            nik                 : null,     name        : null,     gender      : null,
            placeAndDateOfBirth : null,     bloodType   : null,     rtRw        : null,
            address             : null,     village     : null,     district    : null,
            religion            : null,     marital     : null,     nationality : null,
            job                 : null,     validUntil  : null,
        };

        expect(IsKtp(object)).toBe(true)

    });

    let string = {
        "province":"PROVINSI PAPUA",
        "regencyCity":"KABUPATEN YALIMO",
        "issued":"YALIMO, 17-06-2016",
        "nik":"9122030203880004",
        "name":"MARTHEN GOMBO",
        "gender":"LAKI-LAKI",
        "placeAndDateOfBirth":"MELANGGAMA, 02-03-1988",
        "bloodType":"",
        "rtRw":"000",
        "address":"KOLAIMA",
        "village":"KOLAIMA",
        "district":"ABENAHO",
        "religion":"KRISTEN",
        "marital":"KAWIN",
        "nationality":"WNI",
        "job":"PEGAWAI NEGERI SIPIL",
        "validUntil":"SEUMUR HIDUP"
    };

    it("object string form", () => {expect(IsKtp(string)).toBe(true)});

    string["fileName"] = "D:\\programming\\project\\net-one-ocr-server\\dist/../temp/16ce00a9658149498df66c3e0.jpg";
    it("object string form, extra", () => {expect(IsKtp(string)).toBe(true)});
});




describe("invalid", function() {

    it("instantiated", function() {

        expect(IsKtp(new String())).toBe(false)

    });


    it("object", function() {

        let object = {
            province            : null,     regencyCity : null,     issued      : null,
            nik                 : null,     name        : null,     gender      : null,
            placeAndDateOfBirth : null,     bloodType   : null,     rtRw        : null,
            address             : null,     village     : null,     district    : null,
            religion            : null,     marital     : null,     nationality : null,
            job                 : null,     /*validUntil  : null,*/
        };

        expect(IsKtp(object)).toBe(false)

    });
});