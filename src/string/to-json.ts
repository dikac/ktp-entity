import Ktp from "../ktp";
import ToJsonInterface from "lits/dist/string/to-json";
import ToObject from "lits/dist/object/to-object";
import ToString from "lits/dist/string/to-string";

export default class ToJson implements ToObject, ToJsonInterface, ToString {

    constructor(
        private ktp : Ktp
    ) {

    }

    toString(): string {

        return  this.toJson();
    }

    toObject() : Ktp {

        return {
            province           : this.ktp.province,
            regencyCity        : this.ktp.regencyCity,
            issued             : this.ktp.issued,
            nik                : this.ktp.nik,
            name               : this.ktp.name,
            gender             : this.ktp.gender,
            placeAndDateOfBirth: this.ktp.placeAndDateOfBirth,
            bloodType          : this.ktp.bloodType,
            rtRw               : this.ktp.rtRw,
            address            : this.ktp.address,
            village            : this.ktp.village,
            district           : this.ktp.district,
            religion           : this.ktp.religion,
            marital            : this.ktp.marital,
            nationality        : this.ktp.nationality,
            job                : this.ktp.job,
            validUntil         : this.ktp.validUntil,
        }
    }

    toJson () : string {

        return JSON.stringify(this.toObject());
    }
}