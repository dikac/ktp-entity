import Ktp from './ktp';

export default function Flatten(ktp : Ktp) {

    return {
        province            : ktp.province             + '',
        regencyCity         : ktp.regencyCity          + '',
        issued              : ktp.issued               + '',
        nik                 : ktp.nik                  + '',
        name                : ktp.name                 + '',
        gender              : ktp.gender               + '',
        placeAndDateOfBirth : ktp.placeAndDateOfBirth  + '',
        bloodType           : ktp.bloodType            + '',
        rtRw                : ktp.rtRw                 + '',
        address             : ktp.address              + '',
        village             : ktp.village              + '',
        district            : ktp.district             + '',
        religion            : ktp.religion             + '',
        marital             : ktp.marital              + '',
        nationality         : ktp.nationality          + '',
        job                 : ktp.job                  + '',
        validUntil          : ktp.validUntil           + '',
    }
};


