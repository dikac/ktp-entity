import ToString from "lits/dist/string/to-string";

export default interface Ktp {

    /**
     * Level 1 division
     */
    province            : ToString|null;
    /**
     * Level 2 division (kota, kabupaten)
     */
    regencyCity         : ToString|null;
    issued              : ToString|null;
    nik                 : ToString|null;
    name                : ToString|null;
    gender              : ToString|null;
    placeAndDateOfBirth : ToString|null;
    bloodType           : ToString|null;
    rtRw                : ToString|null;
    address             : ToString|null;
    /**
     * Level 4 division (desa, kelurahan)
     */
    village             : ToString|null;
    /**
     * Level 3 division (kecamatan, distrik)
     */
    district            : ToString|null;
    religion            : ToString|null;
    marital             : ToString|null;
    nationality         : ToString|null;
    job                 : ToString|null;
    validUntil          : ToString|null;

};
