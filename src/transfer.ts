import Ktp from './ktp';
import property from "./array/properties";

export default function Transfer<Type extends Ktp = Ktp> (source : Ktp, destination : Type) : Type {

    for(let prop of property) {

        destination[prop] = source[prop];
    }

    return destination;
}


