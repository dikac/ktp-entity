import Ktp from './ktp';
import ToString from 'lits/dist/string/to-string';
import property from "./array/properties";

export default class Standard implements Ktp {

    province            : null|ToString = null;
    regencyCity         : null|ToString = null;
    issued              : null|ToString = null;
    nik                 : null|ToString = null;
    name                : null|ToString = null;
    gender              : null|ToString = null;
    placeAndDateOfBirth : null|ToString = null;
    bloodType           : null|ToString = null;
    rtRw                : null|ToString = null;
    address             : null|ToString = null;
    village             : null|ToString = null;
    district            : null|ToString = null;
    religion            : null|ToString = null;
    marital             : null|ToString = null;
    nationality         : null|ToString = null;
    job                 : null|ToString = null;
    validUntil          : null|ToString = null;

    constructor(data : object = {}) {

        for(let prop of property) {

            let value = data[prop];

            if(value !== undefined) {

                this[prop] = value;
            }

        }
    }

};


