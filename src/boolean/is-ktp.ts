import Ktp from "../ktp";
import property from "../array/properties";


export default function IsKtp (value) : value is Ktp {

    if(typeof value !== 'object' || value === null) {

        return false;
    }

    for(let prop of property) {

        if(!(prop in value)) {

            return false;
        }
    }

    return true;
}